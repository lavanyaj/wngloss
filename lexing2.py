from mynltk import *
from similarity import *
import pickle


def main():
  


  if (sys.argv[1] == 'top'):
    word = sys.argv[2]
    wordidsfile = sys.argv[3]
    worddistfile = sys.argv[4]
    name = sys.argv[5]
    writetopmatrix(word, wordidsfile, worddistfile, name)

  if (sys.argv[1] == 'noun_glosses_with'):
    noun_glosses_with(sys.argv[2])
  if (sys.argv[1] == 'nouns_of'):
    noun_synsets_of(sys.argv[2])

  if (sys.argv[1] == 'sim'):
    write_dissimmatrix(sys.argv[2])
    
  #word = sys.argv[1]
  #write_noun_synsets_of(word)
  #write_noun_synsets_containing(word)
  #writelexmatrix(word)
  return

def noun_synsets_of(word):
  css = [s for s in wn.synsets(word) if s.pos=='n']
  for s in css:
    print(str(s)+": "+s.definition+"\n")
    

          
def noun_glosses_with(word):
  css = [s for s in wn.all_synsets() if is_in(word, s) and s.pos=='n']
  f = open('noun_glosses_with_'+word+'.txt','w')
  for s in css:
    f.write(str(s.lemmas[0])+": "+s.definition+"\n")

  f.close()

def get_wordids(vocabfile):
  f = open(vocabfile, 'r')
  wordids = []
  for line in f:
    wordids.append(line.rstrip())
  f.close()
  print(str(len(wordids)) + " words " + vocabfile)
  return wordids

def get_worddist(worddistfile):
  f = open(worddistfile, 'r')
  worddist = []
  for line in f:
    row = line.rstrip().split()
    items = [float(r) for r in row]
    worddist.append(items)
  f.close()
  print(str(len(worddist)) + " words " + worddistfile)
  return worddist

def writetopmatrices(words, vocabfile, worddistfile):
    for word in words:
        print(word)
        writetopmatrix(word, vocabfile, worddistfile)
    

def writetopmatrix(word, vocabfile, worddistfile, name):
  css = [s for s in wn.all_synsets() if is_in(word, s) and s.pos=='n']
  cns = []
  wordids = get_wordids(vocabfile)
  worddist = get_worddist(worddistfile)
  
  V = len(worddist[0])
  
  f = open(name+'_topmatrix-'+str(len(worddist))+'-'+word+'.txt', 'w')
  vecs = []

  ignored_words = set()
  considered_words = set()
  for s in css:
    postags = nltk.pos_tag(nltk.word_tokenize(s.definition))
    tmp = [0]*V
    count = 0
    for p in postags:
      if (len(wn.synsets(p[0])) > 0 and p[0] != word and str.isalpha(p[0])):
        gword = str.lower(p[0])
        if (gword  not in wordids):
          ignored_words.add(gword)
        else:
          count += 1
          considered_words.add(gword)
          pdist = worddist[wordids.index(gword)]
          _sum = [a+b for a,b in zip(tmp, pdist)]
          tmp = _sum
    if(count==0):
      print(str(s)+" had no taggable words")
    for d in tmp:
      f.write("%.6f " % d),
    f.write("\n")
    vecs.append(tmp)
  f.close()
  
  print(str(len(vecs))+" glosses.\n")
  print("each gloss is a " + str(len(tmp))+ "-vector")
  print(word + " has " + str(len([s for s in wn.synsets(word) if s.pos=='n'])) + " noun synsets")


  f = open('words_used'+str(len(worddist))+'-'+word+'.txt', 'w')
  f.write(str(len(ignored_words)) +" ignored_words\n")

  for word in ignored_words:
    f.write(word+"\n")

  f.write(str(len(considered_words)) +" considered_words\n")
  for word in considered_words:
    f.write(word+"\n")


  f.close()
    
        
  
def writelexmatrix(word): 
  css = [s for s in wn.all_synsets() if is_in(word, s) and s.pos=='n']
  cns = []
  f = open('lex.txt', 'rb')
  lexindex = pickle.load(f)
  f.close()

  f = open('lexmatrix_'+word+'.txt', 'w')

  vecs = []

  for s in css:
    postags = mynltk.pos_tag(nltk.word_tokenize(s.definition))
    tmp = [0]*45
    for p in postags:
      ss =  mynltk.wn_synsets(p[0])
      num = len(ss)
      if (num > 0 and p[0] != word):
  
        for s in ss:
          tmp[lexindex.index(s.lexname)] += 1.0/num
    for d in tmp:
      f.write("%.2f " % d),
    f.write("\n")
    vecs.append(tmp)
  print(str(len(css))+" glosses.\n")
  
  print(word + " has " + str(len([s for s in wn.synsets(word) if s.pos=='n'])) + " noun synsets")
  f.close()

  return

def get_nounsets(word):
  capital_synsets = [syn for syn in list(wn.all_synsets('n')) if mynltk.is_in(word, syn)]
  capital_nouns = []
  for s in capital_synsets:
    postags = mynltk.pos_tag(nltk.word_tokenize(s.definition))
    nouns = [pair[0] for pair in postags if 'NN' in pair[1] and pair[0] != word]
    capital_nouns.append(nouns)
  return capital_nouns


def write_dissimmatrix(word):
  capital_nouns = get_nounsets(word)
  css = [s for s in wn.all_synsets() if is_in(word, s) and s.pos=='n']
  f = open('dissmatrix_'+word+'.txt', 'w')
  n = len(capital_nouns)
  for i in range(n):
    if (i==139 and word=='energy'):
      ni = ['light']
    else:
      ni = capital_nouns[i]

    if(len(ni) == 0):
        print(str(i) + ") " + str(css[i].definition))

    for j in range(n):
      if (j==139 and word=='energy'):
        nj = ['light']
      else:
        nj = capital_nouns[j]

      if (len(ni) * len(nj) == 0):        
        dissim = 0
      else:
        dissim = nounset_similarity(ni, nj)
        dissim2 = nounset_similarity(nj, ni)
        
      f.write(str(dissim * 10000)+" ")
    f.write("\n")

  f.close()
                                                            
if __name__ == '__main__':
  main()
  
