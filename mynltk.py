from nltk.corpus import wordnet as wn
import string
import re
import nltk
sentence = 'The tabby cat jumped over the moon'

def wn_synsets(word):
    try1 = wn.synsets(word)
    if ('_' not in word or len(try1) > 0):
        return try1
    word2 = word[word.find('_')+1:]
    try2 = wn.synsets(word2)
#    if ('_' not in word2 or len(try2) > 0):
    return try2

def is_in(word, syn):
    # will not get hyphenated occurences like lime-tree or tree-house
    # 'tree! tree? yes, tree-house in a tree'
    # 'tree tree yes treehouse in a tree'
    no_punct = re.sub('[%s]' % re.escape(string.punctuation), '', syn.definition)
    matched = re.search(r'(^|\s+)'+word+'($|\s+)', no_punct)
    if (matched is None):
        return False
    return True
            
def pos_tag(tokens):
    postags = nltk.pos_tag(tokens)
    p = 0
    new_postags = []
    noun_tmp_word = None
    noun_tmp_pos = None
    while(p < len(postags)):
        #print("Looking at " + str(postags[p]))
        #print("noun_tmp is " + str(noun_tmp_word))
        if ('NN' not in postags[p][1] or p == len(postags)-1):
            if (noun_tmp_word is not None):
                #print("appending " + str(noun_tmp))
                t = (noun_tmp_word, noun_tmp_pos)
                new_postags.append(t)
            #print("appending " + str(postags[p]))
            if (len(new_postags) == 0):
                new_postags = [postags[p]]
            else:
                new_postags.append(postags[p])
            noun_tmp_word = None
        else:
            if (noun_tmp_word is not None):
                noun_tmp_word += '_'+postags[p][0]
            else:
                noun_tmp_word = postags[p][0]
            noun_tmp_pos = postags[p][1]
        p+=1
    return new_postags
