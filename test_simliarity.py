from lexing2 import *
word = 'space'
capital_nouns = get_nounsets(word)
n = len(capital_nouns)
for i in range(n):
    for j in range(n):
        ni = capital_nouns[i]
        nj = capital_nouns[j]
        if (len(ni) * len(nj) == 0):
            print(str(i) + " or " + str(j) + " is empty.")
        if (nounset_similarity(ni, nj)*1000<0):
            print(str(i)+")"+str(ni))
            print(str(j)+")"+str(nj))
            print("---")
