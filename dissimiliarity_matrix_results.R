#Read in manual labels

capital_truth <- read.table("capital_manual.txt", header = F, as.is = T)
capital_truth <- as.matrix(capital_truth)
capital_truth <- capital_truth[, 1]

space_truth <- read.table("space_manual.txt", header = F, as.is = T)
space_truth <- as.matrix(space_truth)
space_truth <- space_truth[, 1]

bow_truth <- read.table("bow_manual.txt", header = F, as.is = T)
bow_truth <- as.matrix(bow_truth)
bow_truth <- bow_truth[, 1]

bank_truth <- read.table("bank_manual.txt", header = F, as.is = T)
bank_truth <- as.matrix(bank_truth)
bank_truth <- bank_truth[, 1]

crane_truth <- read.table("crane_manual.txt", header = F, as.is = T)
crane_truth <- as.matrix(crane_truth)
crane_truth <- crane_truth[, 1]

coach_truth <- read.table("coach_manual.txt", header = F, as.is = T)
coach_truth <- as.matrix(coach_truth)
coach_truth <- coach_truth[, 1]

energy_truth <- read.table("energy_manual.txt", header = F, as.is = T)
energy_truth <- as.matrix(energy_truth)
energy_truth <- energy_truth[, 1]

foot_truth <- read.table("foot_manual.txt", header = F, as.is = T)
foot_truth <- as.matrix(foot_truth)
foot_truth <- foot_truth[, 1]

pound_truth <- read.table("pound_manual.txt", header = F, as.is = T)
pound_truth <- as.matrix(pound_truth)
pound_truth <- pound_truth[, 1]

race_truth <- read.table("race_manual.txt", header = F, as.is = T)
race_truth <- as.matrix(race_truth)
race_truth <- race_truth[, 1]

star_truth <- read.table("star_manual.txt", header = F, as.is = T)
star_truth <- as.matrix(star_truth)
star_truth <- star_truth[, 1]

#run K-medoids algorithm and print results

dis_bank_assign <- get_km_clusters("dissmatrix_bank.txt", "noun_glosses_with_bank.txt", 2)
print.results(2, 3, dis_bank_assign, bank_truth)
cat("Quality of clustering: ", nmi(dis_bank_assign, bank_truth, 2))

dis_bow_assign <- get_km_clusters("dissmatrix_bow.txt", "noun_glosses_with_bow.txt", 5)
print.results(5, 5, dis_bow_assign, bow_truth)
cat("Quality of clustering: ", nmi(dis_bow_assign, bow_truth, 5))


dis_capital_assign <- get_km_clusters("dissmatrix_capital.txt", "noun_glosses_with_capital.txt", 2)
print.results(2, 3, dis_capital_assign, capital_truth)
cat("Quality of clustering: ", nmi(dis_capital_assign, capital_truth, 2))

dis_coach_assign <- get_km_clusters("dissmatrix_coach.txt", "noun_glosses_with_coach.txt", 2)
print.results(2, 2, dis_coach_assign, coach_truth)
cat("Quality of clustering: ", nmi(dis_coach_assign, coach_truth, 2))

dis_crane_assign <- get_km_clusters("dissmatrix_crane.txt", "noun_glosses_with_crane.txt", 2)
print.results(2, 3, dis_crane_assign, crane_truth)
cat("Quality of clustering: ", nmi(dis_crane_assign, crane_truth, 2))


dis_energy_assign <- get_km_clusters("dissmatrix_energy.txt", "noun_glosses_with_energy.txt", 2)
print.results(2, 3, dis_energy_assign, energy_truth)

dis_foot_assign <- get_km_clusters("dissmatrix_foot.txt", "noun_glosses_with_foot.txt", 2)
print.results(2, 3, dis_foot_assign, foot_truth)

dis_pound_assign <- get_km_clusters("dissmatrix_pound.txt", "noun_glosses_with_pound.txt", 2)
print.results(2, 2, dis_pound_assign, pound_truth)

dis_race_assign <- get_km_clusters("dissmatrix_race.txt", "noun_glosses_with_race.txt", 2)
print.results(2, 3, dis_race_assign, race_truth)

dis_space_assign <- get_km_clusters("dissmatrix_space.txt", "noun_glosses_with_space.txt", 2)
print.results(2, 3, dis_space_assign, space_truth)

dis_star_assign <- get_km_clusters("dissmatrix_star.txt", "noun_glosses_with_star.txt", 3)
print.results(3, 4, dis_star_assign, star_truth)
