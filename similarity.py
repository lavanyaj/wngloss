#!/usr/bin/env python
# encoding: utf-8
import string
import mynltk
import re
import sys
import os
from nltk.corpus import wordnet as wn
import numpy as np
import nltk

def main():


    tree_synsets = [syn for syn in list(wn.all_synsets('n')) if mynltk.is_in('tree', syn)]
    tree_nouns = []

    for s in tree_synsets:
        postags = mynltk.pos_tag(nltk.word_tokenize(s.definition))
        nouns = [pair[0] for pair in postags if 'NN' in pair[1]]
        
        tree_nouns.append(nouns)
    tree20 = tree_nouns[1:20]        

    similiarity_matrix = np.arange(400).reshape(20,20)
    pass

def is_in(word, syn):
    # will not get hyphenated occurences like lime-tree or tree-house
    # 'tree! tree? yes, tree-house in a tree'
    # 'tree tree yes treehouse in a tree'
    no_punct = re.sub('[%s]' % re.escape(string.punctuation), '', syn.definition)
    matched = re.search(r'(^|\s+)'+word+'($|\s+)', no_punct)
    if (matched is None):
        return False
    return True
    

def nounset_similarity(nounset1, nounset2):    
    threshold = 0.07
    total_distance = 0
    count_pairs = 0
    if (len(nounset1)*len(nounset2)==0):
        print("empty nounsets to nounset_similarity")
    for n1 in nounset1:
        for n2 in nounset2:
#            print("comparing "+ n1 + " and " + n2)
            similarity = word_similarity(n1, n2)
#            print("similarity is " + str(similarity))
            count_pairs += 1
            if (similarity[0] < threshold):
                continue            
            total_distance += similarity[0]


    return total_distance/count_pairs

'''
if either word's not in wordnet nouns, returns [0, None, None]
'''
def word_similarity(word1, word2):
    ws1 = mynltk.wn_synsets(word1)
    ws2 = mynltk.wn_synsets(word2)
    synsets1 =  [s for s in ws1 if s.pos is 'n']
    synsets2 =  [s for s in ws2 if s.pos is 'n']
    if (len(synsets1)*len(synsets2) is 0):
        return [0, None, None, word1, word2]

    max_similarity = -2
    best_s1 = None
    best_s2 = None
    for s1 in synsets1:
        for s2 in synsets2:
            similarity = s1.path_similarity(s2)
            if (similarity is None):
                print("distance is None " + s1 + ", " + s2)
            if (similarity > max_similarity ):
                max_similarity = similarity
                best_s1 = s1
                best_s2 = s2    
    return [max_similarity, best_s1, best_s2, word1, word2]

if __name__ == '__main__':
	main()

