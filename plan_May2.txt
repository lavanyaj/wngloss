To Discuss with Professor Blei 
Evaluation: what do we do?
 -How can we devise a way to assess the quality of a clustering? Relating to entropy? But entropies of individual topics don’t capture how mixed the topics are within clusters.
 -Proposed approach: find entropies of clusters over topics. Then weight cluster entropies by sizes of clusters. 
 -Cross-validation. Do folds?
 -Perplexity?
 -Calibrating: what are the knobs?
 -Threshold
 -Value of K (maybe – but not sure)
 -Number of topics
 -Number of articles per batch
 -Threshold (first n, or value threshold) for the feature vectors. The rest of the stuff is just noise, right?
 -General questions	
 -Are we using the right distributions? Should we try other distributions?
 -What is the effect of documents (glosses) of different lengths in M.O.M.? Like assignment 2. Can we try to plug this in for mixture of Gaussians?
